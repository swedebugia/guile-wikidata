;;; Copyright © 2017 Roel Janssen <roel@gnu.org>
;;; Copyright © 2018 swedebugia <swedebugia@riseup.net>
;;;
;;; This file is part of guile-wikidata.
;;;
;;; guile-wikidata is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; guile-wikidata is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with guile-wikidata.  If not, see <http://www.gnu.org/licenses/>.

;; The following step is not needed with Guix:
;; We must add the (wikidata sparql) module to GNU Guile's load path
;; before we can load it.  This means we have to add the directory
;; that leads to the sparql/ directory to the %load-path variable.
; (add-to-load-path "/path/to/the/root/of/the/repository")

;; We need the following modules to show the query.
(use-modules (ice-9 format)
             (wikidata sparql))

;; Example query passed to the screen for you to eyeball
(show-sparql
 ;; FIXME why is LIMIT not honored?
 (format #f "
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX wdt: <http://www.wikidata.org/prop/direct/>
SELECT DISTINCT ?item 	   
WHERE \\{?item wdt:P31/wdt:P279* wd:Q19723451\\}
LIMIT 10
"))

;; See more example queries at
;; https://www.wikidata.org/wiki/Wikidata:SPARQL_query_service/queries/examples

;;; To generate SPARQL queries I recommend
;;; https://query.wikidata.org/. It is excellent because it looks up
;;; all the P- and Q-ids for you and generate the SPARQL for you to
;;; insert above. :D


